GitLab Docker
=========

Spawns a gitlab-ce docker container

Role Variables
--------------

| Name                     | Default Value      | Usage                                                 |
|--------------------------|--------------------|-------------------------------------------------------|
| gitlab_project_directory | '/srv/gitlab'      | Directory on the host where the GitLab data is stored |
| gitlab_container_name    | 'gitlab-server'    | Name of the GitLab instance                           |
| gitlab_base_image        | 'gitlab/gitlab-ce' | Docker base image        i                            |
| gitlab_expose_ssh        | true               | If true ssh port will be exposed                      |
| gitlab_expose_http       | true               | If true http port will be exposed                     |
| gitlab_expose_https      | true               | If true https port will be exposed                    |
| gitlab_ssh_port          | 22                 | External ssh port                                     |
| gitlab_http_port         | 80                 | External http port                                    |
| gitlab_https_port        | 443                | External https port                                   |

Example
-------

```bash
/usr/bin/ssh -p 2222 -nNTf -L /tmp/remote-docker.sock:/var/run/docker.sock root@<<<REMOTE-HOST>>>
ansible-playbook --vault-password-file $ROOT/.vpass $DIR/dockerhosts.yml
killall ssh
rm -rf /tmp/remote-docker.sock
```

License
-------

MIT
